all:
	$(MAKE) -C src/

run:
	cd src/ && make run

clean:
	cd src/ && make clean

jar:
	cd src/ && make jar

jrun:
	java -jar Software.jar
